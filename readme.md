<h1 align="center">It Crowd Test</h1>

##About

Technical interview test to evaluate my web development skills.

##Problem
A company that has a website about movies wants to provide its customers and users an API to query their database, as well as provide the trusted company users the ability to update or create new records.

##Requirements

- PHP >= 7.1.3
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- BCMath PHP Extension

##Installation

After cloning the repository, move to the new project folder, edit copy .env-example .env file and set your Database 
credentials. Then run the next commands:

- composer install
- php artisan key:generate
- php artisan migrate --seed
- php artisan passport:install
    * Set the password client id and password client secret to .env file keys

##Documentation

https://documenter.getpostman.com/view/4740614/S1LyVTnU?version=latest
