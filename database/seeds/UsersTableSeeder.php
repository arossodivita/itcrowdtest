<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate([
                'email' => 'admin@itcrowd.local'
            ],
            [
                'name' => 'Maria Cuervo',
                'email' => 'admin@itcrowd.local',
                'password' => Hash::make('itCrowdAdmin2019'),
            ]
        );
    }
}
