<?php

return [
    /*
	|--------------------------------------------------------------------------
	| OAuth
	|--------------------------------------------------------------------------
	|
	| Config to serve OAuth2 authentication
	|
	*/

    'OAUTH' => [
        'GRANT_TYPE' => [
            'PASSWORD' => env('OAUTH_GRANT_TYPE_PASSWORD'),
            'REFRESH_TOKEN' => env('OAUTH_GRANT_TYPE_REFRESH_TOKEN'),
        ],
        'CLIENT_ID' => env('OAUTH_CLIENT_ID'),
        'CLIENT_SECRET' => env('OAUTH_CLIENT_SECRET'),
    ],

    /*
	|--------------------------------------------------------------------------
	| Roles
	|--------------------------------------------------------------------------
	|
	| Different person roles in a movie
	|
	*/

    'ROLES' => [
        'ACTOR' => 'Actor/Actress',
        'DIRECTOR' => 'Director',
        'PRODUCER' => 'Producer',
    ],
];