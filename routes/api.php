<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Login Routes
Route::post('/login', 'Api\AuthController@login');
Route::post('/refresh-token', 'Api\AuthController@refreshToken');

//Safe Routes
Route::get('/persons', 'Api\PersonController@index');
Route::get('/persons/{person}', 'Api\PersonController@show');

Route::get('/movies', 'Api\MovieController@index');
Route::get('/movies/{movie}', 'Api\MovieController@show');

//Authenticated Routes
Route::middleware('auth:api')->group(function () {
    //Users
    Route::post('/logout', 'Api\AuthController@logout');

    //Persons
    Route::post('/persons', 'Api\PersonController@store');
    Route::put('/persons/{person}', 'Api\PersonController@update');
    Route::delete('/persons/{person}', 'Api\PersonController@destroy');
    Route::post('/persons/{person}/movie', 'Api\PersonController@setMovie');
    Route::delete('/persons/{person}/movie/detach', 'Api\PersonController@removeMovie');

    //Movies
    Route::post('/movies', 'Api\MovieController@store');
    Route::put('/movies/{movie}', 'Api\MovieController@update');
    Route::delete('/movies/{movie}', 'Api\MovieController@destroy');
});
