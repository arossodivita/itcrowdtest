<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    const ACTOR = 'ACTOR';

    const DIRECTOR = 'DIRECTOR';

    const PRODUCER = 'PRODUCER';

    protected $table = 'persons';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'last_name', 'first_name', 'aliases',
    ];

    protected $hidden = [
        'pivot',
    ];

    public function movies()
    {
        return $this->belongsToMany('App\Movie')->withPivot('role');
    }

    public function moviesAsActor()
    {
        return $this->belongsToMany('App\Movie')->wherePivot('role', self::ACTOR);
    }

    public function moviesAsDirector()
    {
        return $this->belongsToMany('App\Movie')->wherePivot('role', self::DIRECTOR);
    }

    public function moviesAsProducer()
    {
        return $this->belongsToMany('App\Movie')->wherePivot('role', self::PRODUCER);
    }
}
