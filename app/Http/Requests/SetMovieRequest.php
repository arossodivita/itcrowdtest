<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SetMovieRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'movie_id' => 'required|exists:movies,id',
            'role'  => 'required|string|max:255',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if (!in_array(Request()->get('role'), array_keys(config('constants.ROLES')))) {
                $validator->errors()->add('role', 'the given role do not exist');
            }
        });
    }
}
