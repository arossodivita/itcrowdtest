<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\RemoveMovieRequest;
use App\Person;
use App\Http\Controllers\Controller;
use App\Http\Requests\SetMovieRequest;
use App\Http\Requests\StorePersonRequest;
use App\Http\Requests\UpdatePersonRequest;

class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response()->json(
            Person::with([
                'moviesAsActor:id,title,release_year',
                'moviesAsDirector:id,title,release_year',
                'moviesAsProducer:id,title,release_year'])->get(),
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StorePersonRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePersonRequest $request)
    {
        $person = Person::create($request->all());

        return Response()->json($person, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function show(Person $person)
    {
        return Response()->json($person->load(
            [
                'moviesAsActor:id,title,release_year',
                'moviesAsDirector:id,title,release_year',
                'moviesAsProducer:id,title,release_year'
            ]),
            200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdatePersonRequest $request
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePersonRequest $request, Person $person)
    {
        $person->update($request->all());

        return Response()->json($person, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     * @throws
     */
    public function destroy(Person $person)
    {
        $person->moviesAsActor()->detach();
        $person->moviesAsDirector()->detach();
        $person->moviesAsProducer()->detach();

        $person->delete();

        return Response()->json(['message' => 'deleted'], 200);
    }

    /**
     * Set person movie with role
     *
     * @param SetMovieRequest $request
     * @param Person $person
     * @return \Illuminate\Http\JsonResponse
     */
    public function setMovie(SetMovieRequest $request, Person $person)
    {
        $person->movies()->syncWithoutDetaching([$request->get('movie_id') => ['role' => $request->get('role')]]);

        return Response()->json(
            $person->load(
                [
                    'moviesAsActor:id,title,release_year',
                    'moviesAsDirector:id,title,release_year',
                    'moviesAsProducer:id,title,release_year'
                ]),
            200
        );
    }

    /**
     * Remove user movie
     *
     * @param RemoveMovieRequest $request
     * @param Person $person
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeMovie(RemoveMovieRequest $request, Person $person)
    {
        $person->movies()->detach($request->get('movie_id'));

        return Response()->json(['message' => 'removed'], 200);
    }
}
