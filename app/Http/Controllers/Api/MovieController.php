<?php

namespace App\Http\Controllers\Api;

use App\Movie;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreMovieRequest;
use App\Http\Requests\UpdateMovieRequest;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response()->json(
            Movie::with([
                'casting:id,last_name,first_name,aliases',
                'directors:id,last_name,first_name,aliases',
                'producers:id,last_name,first_name,aliases'])->get(),
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreMovieRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMovieRequest $request)
    {
        $movie = Movie::create($request->all());

        return Response()->json($movie, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function show(Movie $movie)
    {
        return Response()->json($movie->load(
            [
                'casting:id,last_name,first_name,aliases',
                'directors:id,last_name,first_name,aliases',
                'producers:id,last_name,first_name,aliases'
            ]),
            200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateMovieRequest $request
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMovieRequest $request, Movie $movie)
    {
        $movie->update($request->all());

        return Response()->json($movie, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     * @throws
     */
    public function destroy(Movie $movie)
    {
        $movie->casting()->detach();
        $movie->directors()->detach();
        $movie->producers()->detach();

        $movie->delete();

        return Response()->json(['message' => 'deleted'], 200);
    }
}
