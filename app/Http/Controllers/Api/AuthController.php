<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AuthController extends Controller
{
    /**
     * Login user with given credentials
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        try {
            $validator = $this->validator($request->all());

            if ($validator->fails()) {
                return Response()->json($validator->errors(), 422);
            }

            $user = User::whereEmail($request->get('username'))->firstOrFail();

            $oauthRequest = $this->buildOauthRequest($request,'token');

            $response = $this->handleOauthResponse(
                app()->handle(Request::create('/oauth/token', 'POST', $oauthRequest))
            );

            return Response()->json(
                $response ? [
                    'access_token' => $response->access_token,
                    'refresh_token' => $response->refresh_token,
                    'user' => $user->toArray(),
                ] : ['message' => 'Unauthorized']
                ,
                $response ? 200 : 401
            );

        } catch (ModelNotFoundException $exception) {
            return Response()->json(['message' => 'User Not Found'], 404);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $request->user()->token()->id)
            ->update([
                'revoked' => true,
            ]);

        $request->user()->token()->revoke();

        return Response()->json(['message' => 'success'], 200);
    }

    /**
     * Refresh a given access token
     *
     * @param Request $request
     * @return mixed
     */
    public function refreshToken(Request $request)
    {
        if (!$request->get('refresh_token')) {
            return Response()->json(['message' => 'refresh_token is required'], 422);
        }

        $oauthRequest = $this->buildOauthRequest($request, 'refresh');

        $request = Request::create('/oauth/token', 'POST', $oauthRequest);

        return app()->handle($request);
    }

    /**
     * Handle the OAuth2 Response
     *
     * @param $response
     * @return mixed
     */
    private function handleOauthResponse($response)
    {
        // Check if the request was successful
        if ($response->getStatusCode() != 200) {
            return null;
        }

        // Get the data from the response
        return json_decode($response->getContent());
    }

    /**
     * Build Oauth2 request array
     *
     * @param Request $request
     * @param $type
     * @return array
     */
    private function buildOauthRequest(Request $request, $type)
    {
        $data = [
            'client_id' => config('constants.OAUTH.CLIENT_ID'),
            'client_secret' => config('constants.OAUTH.CLIENT_SECRET'),
        ];

        switch ($type) {
            case 'token':
                $data = $data + [
                    'grant_type' => config('constants.OAUTH.GRANT_TYPE.PASSWORD'),
                    'username' => $request->get('username'),
                    'password' => $request->get('password'),
                ];
                break;

            default:
                $data = $data + [
                    'grant_type' => config('constants.OAUTH.GRANT_TYPE.REFRESH_TOKEN'),
                    'refresh_token' => $request->get('refresh_token'),
                ];
                break;
        }

        return $data;
    }

    /**
     * Validate user fields
     *
     * @param Array $data
     * @param Int $calendar
     * @return \Illuminate\Support\Facades\Validator
     */
    private function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|string|max:255',
            'password' => 'required|string|min:8',
        ]);
    }
}
