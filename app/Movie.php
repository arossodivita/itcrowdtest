<?php

namespace App;

use Romans\Filter\IntToRoman;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    const ACTOR = 'ACTOR';

    const DIRECTOR = 'DIRECTOR';

    const PRODUCER = 'PRODUCER';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'release_year',
    ];

    protected $hidden = [
        'pivot',
    ];

    protected $appends = [
        'roman_year'
    ];

    public function getRomanYearAttribute()
    {
        $converter = new IntToRoman();

        return $converter->filter($this->release_year);
    }

    public function casting()
    {
        return $this->belongsToMany('App\Person')->wherePivot('role', self::ACTOR);
    }

    public function directors()
    {
        return $this->belongsToMany('App\Person')->wherePivot('role', self::DIRECTOR);
    }

    public function producers()
    {
        return $this->belongsToMany('App\Person')->wherePivot('role', self::PRODUCER);
    }
}
